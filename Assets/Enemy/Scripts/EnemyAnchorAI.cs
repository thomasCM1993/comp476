﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class EnemyAnchorAI : MonoBehaviour
{

    private List<Vector3> targets; // list of target positions
    private int indexOfTargets;
    public float chasingTime;
    private float notSeePlayerTime = 0;

    private float targetPointPositionY = 1.5f;
    public GameObject shootWhichPlayer { get; set; }

    public float viewAngle;
    private List<GameObject> players = new List<GameObject>();


    public GameObject elephantPrefab;
    public GameObject bearPrefab;
    public GameObject bunnyPrefab;

    public bool isFiring { get; set; }
    private List<GameObject> enemies = new List<GameObject>();
    

    Rigidbody rb;
    NavMeshAgent nvAgent;
    Vector3 mVelocity = Vector3.zero;

    void Awake()
    {
        // InitializeTargetPoints();
        rb = GetComponent<Rigidbody>();
        nvAgent = GetComponent<NavMeshAgent>();
        isFiring = false;
    }

    // Use this for initialization
    void Start()
    {
        InitializePlayers();
        InitializeEnemies();
        
    }

    // Update is called once per frame
    void Update()
    {
        updateTarget();
        updateAnchorBehaviour();
    }


    void InitializeTargetPoints()
    {
        targets = new List<Vector3>();

        targets.Add(new Vector3(268.835f, targetPointPositionY, 165.3f));
        targets.Add(new Vector3(230.438f, targetPointPositionY, 165.64f));
        targets.Add(new Vector3(234.435f, targetPointPositionY, 197.01f));
        targets.Add(new Vector3(270.5f, targetPointPositionY, 196.194f));
    }

    public void SetTargetPoints(List<Vector3> targetPoints)
    {
        targets = targetPoints;
    }

    void InitializePlayers()
    {
        players.AddRange(GameObject.FindGameObjectsWithTag("Player"));
        players.AddRange(GameObject.FindGameObjectsWithTag("PlayerCoop"));
    }

    public void InitializeAnchorPosition(int index)
    {
        nvAgent.enabled = false;
        index = index % targets.Count;
        transform.position = targets[index];
        indexOfTargets = index;
        nvAgent.enabled = true;
    }

    void InitializeEnemies()
    {
        //elephants
        for(int i = 0; i < 2; i++)
        {
            Vector3 relaPos = new Vector3(-2 + 4 * i, 0, 2.0f);
            GameObject anElephant = (GameObject)Instantiate(elephantPrefab, transform.TransformPoint(relaPos), transform.rotation);
            EnemyBehaviour eb = anElephant.GetComponent<EnemyBehaviour>();
            eb.mAnchor = this;
            eb.relativePos = relaPos;
            enemies.Add(anElephant);
        }

        //bears
        for (int i = 0; i < 3; i++)
        {
            Vector3 relaPos = new Vector3(-2 + 2 * i, 0, 0.0f);
            GameObject aBear = (GameObject)Instantiate(bearPrefab, transform.TransformPoint(relaPos), transform.rotation);
            EnemyBehaviour eb = aBear.GetComponent<EnemyBehaviour>();
            eb.mAnchor = this;
            eb.relativePos = relaPos;
            enemies.Add(aBear);
        }

        //bunnies
        for (int i = 0; i < 3; i++)
        {
            Vector3 relaPos = new Vector3(-2 + 2 * i, 0, -2.0f);
            GameObject aBunny = (GameObject)Instantiate(bunnyPrefab, transform.TransformPoint(relaPos), transform.rotation);
            EnemyBehaviour eb = aBunny.GetComponent<EnemyBehaviour>();
            eb.mAnchor = this;
            eb.relativePos = relaPos;
            enemies.Add(aBunny);
        }
    }

    void updateTarget()
    {
        if ((targets[indexOfTargets] - transform.position).magnitude < 1.0f)
        {
            // arrive this target, set new target
            int newIndexOfTargets = (indexOfTargets + 1) % (targets.Count);
            indexOfTargets = newIndexOfTargets;

            //Debug.Log("update target");
        }
    }

    void updateAnchorBehaviour()
    {
        if (CheckVisibility()) // shoot at the player
        {
            notSeePlayerTime = 0;
            nvAgent.SetDestination(shootWhichPlayer.transform.position);
            GetComponent<Rigidbody>().rotation = Quaternion.LookRotation((shootWhichPlayer.transform.position - transform.position).normalized);
            GetComponent<Renderer>().material.color = Color.red;
            nvAgent.speed = 4.0f;
            isFiring = true;

            //Debug.Log("See player, turn red");
        }
        else // move around
        {
            if (notSeePlayerTime < chasingTime && shootWhichPlayer!=null)
            {
                notSeePlayerTime += Time.deltaTime;
                nvAgent.SetDestination(shootWhichPlayer.transform.position);
                GetComponent<Rigidbody>().rotation = Quaternion.LookRotation((shootWhichPlayer.transform.position - transform.position).normalized);
                GetComponent<Renderer>().material.color = Color.red;
                nvAgent.speed = 4.0f;
                isFiring = true;

                //Debug.Log("Not see player, turn red, still chase");

            }
            else
            {
                notSeePlayerTime = 0.0f;
                setWhichPlayerToShoot(null);
                nvAgent.SetDestination(targets[indexOfTargets]);
                GetComponent<Renderer>().material.color = Color.blue;
                nvAgent.speed = 2.5f;
                isFiring = false;
                //Debug.Log("Didn't see player. move around");
            }
           
        }
    }

    private bool CheckVisibility()
    {
        bool result = false;
        float maxRange = 20.0f;
        RaycastHit hit;

        List<GameObject> playersSeen = new List<GameObject>();

        for(int i = 0; i < players.Count; i++)
        {
            GameObject aPlayer = players[i];
            Vector3 targetDirection = (aPlayer.transform.position - this.transform.position).normalized;
            float cosTheta=Vector3.Dot(targetDirection, transform.TransformDirection(Vector3.forward));
            if (Mathf.Acos(cosTheta) < viewAngle/180*Mathf.PI)
            {
                if (Physics.Raycast(transform.position, targetDirection, out hit, maxRange))
                {
                    if (hit.transform.gameObject.tag.Contains("Player"))
                    {
                        playersSeen.Add(hit.transform.gameObject);
                        result = true;
                    }

                    //Debug.Log("check visibility, have seen player.");
                }
            }
        }


        if (result == true)
        {
            //int randomNum = Random.Range(0, playersSeen.Count);
            //setWhichPlayerToShoot(playersSeen[randomNum]);

            float distance = Mathf.Infinity;
            GameObject shootPlayer=null;
            for(int i = 0; i < playersSeen.Count; i++)
            {
                if ((playersSeen[i].transform.position - this.transform.position).magnitude < distance)
                {
                    distance = (playersSeen[i].transform.position - this.transform.position).magnitude;
                    shootPlayer = playersSeen[i];
                }
            }
            setWhichPlayerToShoot(shootPlayer);
        }
        else
        {
            //setWhichPlayerToShoot(null);
        }

        return result;
    }

    private void setWhichPlayerToShoot(GameObject shootThisPlayer)
    {
        shootWhichPlayer = shootThisPlayer;
    }


}
