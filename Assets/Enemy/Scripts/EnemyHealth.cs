﻿using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public float sinkSpeed = 2.5f;
    public int scoreValue = 10;
    public AudioClip deathClip;


    Animator anim;
    AudioSource enemyAudio;
    ParticleSystem hitParticles;
    CapsuleCollider capsuleCollider;
    public bool isDead { get; set; }
    bool isSinking;
    Slider healthBar;

    public GameObject Progess;

    void Awake ()
    {
        anim = GetComponent <Animator> ();
        enemyAudio = GetComponent <AudioSource> ();
        hitParticles = GetComponentInChildren <ParticleSystem> ();
        capsuleCollider = GetComponent <CapsuleCollider> ();
        healthBar = GetComponentInChildren<Slider>();

        currentHealth = startingHealth;
        healthBar.maxValue = startingHealth;
        healthBar.value = startingHealth;
        if(transform.position.y < 35f && transform.position.z < 220f)
        {
            Progess.GetComponent<ProgressionScript>().IncreaseCount();
        }
        else
        {
            Progess.GetComponent<ProgressionScript>().IncreaseOtherCount();
        }
    }


    void Update ()
    {
        if(isSinking)
        {
            transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
        }
        UpdateHealthBar();
    }


    public void TakeDamage (int amount, Vector3 hitPoint)
    {
        if(isDead)
            return;

        enemyAudio.Play ();

        currentHealth -= amount;
            
        hitParticles.transform.position = hitPoint;
        hitParticles.Play();

        if(currentHealth <= 0)
        {
            Death ();
        }
    }


    void Death ()
    {
        isDead = true;

        capsuleCollider.isTrigger = true;

        anim.SetTrigger ("Dead");

        enemyAudio.clip = deathClip;
        enemyAudio.Play ();
        if (transform.position.y < 35f && transform.position.z < 220f)
        {
            Progess.GetComponent<ProgressionScript>().DecreaseCount();
        }
        else
        {
            Progess.GetComponent<ProgressionScript>().DecreaseOtherCount();
        }
        //don't remove, we dont always want drops
        float rand = Random.Range(0f, 15f);
        if(rand <=5)
        {
            GameObject explo = (GameObject)Instantiate(Resources.Load("PowerUpContainerGreen"), transform.position + Vector3.up, Quaternion.identity);
        }
        else if(rand >5 && rand <= 10)
        {
            GameObject explo = (GameObject)Instantiate(Resources.Load("Ammo"), transform.position + Vector3.up, Quaternion.identity);
        }
    }


    public void StartSinking ()
    {
        GetComponent <NavMeshAgent> ().enabled = false;
        GetComponent <Rigidbody> ().isKinematic = true;
        isSinking = true;
        //ScoreManager.score += scoreValue;
        Destroy (gameObject, 2f);
    }

    void UpdateHealthBar()
    {
        healthBar.value = currentHealth;
        Camera mCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        healthBar.transform.parent.rotation = Quaternion.LookRotation(mCamera.transform.TransformDirection(Vector3.forward));
        if (currentHealth > startingHealth * 0.7f)
        {
            healthBar.transform.FindChild("Fill Area").FindChild("Fill").GetComponent<Image>().color = Color.green;
        }
        else if (currentHealth > startingHealth * 0.3f)
        {
            healthBar.transform.FindChild("Fill Area").FindChild("Fill").GetComponent<Image>().color = Color.yellow;
        }
        else
        {
            healthBar.transform.FindChild("Fill Area").FindChild("Fill").GetComponent<Image>().color = Color.red;
        }
    }
}
