﻿using UnityEngine;
using System.Collections;

public class Meteor : MonoBehaviour {

    public ParticleSystem ShrapnelParticleSystem;
    public ParticleSystem explosionParticleSystem;
    public AudioClip emissionSound;
    public AudioClip explosionSound;

    public int damage { get; set; }

    AudioSource audio;
    Vector3 targetPosition;
    bool exploded = false;

    void Awake()
    {
        audio = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start () {
        audio.clip = emissionSound;
        audio.Play();
    }
	
   
	// Update is called once per frame
	void Update () {
        if (transform.position.y < targetPosition.y)
        {
            if (exploded == false)
            {
                exploded = true;
                GetComponent<Rigidbody>().isKinematic = true;
                Explode();
            }
            
        }
	}


    public void SetTargetPosition(Vector3 targetPos)
    {
        targetPosition = targetPos;
        GetComponent<Rigidbody>().velocity = (targetPos - this.transform.position).normalized * 10.0f;
    }


    void Explode()
    {
        audio.clip = explosionSound;
        audio.Play();
        GetComponent<MeshRenderer>().enabled = false;
        ShrapnelParticleSystem.Emit(20);
        explosionParticleSystem.Emit(20);
        Destroy(this.gameObject, 3.0f);
    }

    void OnTriggerEnter(Collider other)
    {
        if (!other.tag.Contains("Player"))
        {
            return;
        }

        //player
        if (other.GetComponent<PlayerAttributes>())
        {
            other.GetComponent<PlayerAttributes>().TakeDamage(damage);
        }
        //npc
        if (other.GetComponent<coop_stats>())
        {
            other.GetComponent<coop_stats>().TakeDamage(damage);
        }

    }
}
