﻿using UnityEngine;
using System.Collections;

public class EnemyBehaviour : MonoBehaviour
{

    public EnemyAnchorAI mAnchor { get; set; }
    public Vector3 relativePos { get; set; }
    public float shootingInterval;
    public GameObject BunnyMagicEffectPrefab;
    public float BunnyMagicTime;
    public float BunnyMagicInterval;
    public GameObject MagicCirclePrefab;
    public GameObject MeteorPrefab;

    public int ElephantDamage = 5;
    public int BearDamage = 2;
    public int BunnyMeteorDamage = 10;

    private float timer = 0;
    NavMeshAgent nvAgent;
    EnemyHealth healthScript;



    //bear
    LineRenderer gunLine;

    //bunny
    GameObject BunnyMagicEffect;
    GameObject MagicCircle;



    void Awake()
    {
        nvAgent = GetComponent<NavMeshAgent>();

        if (this.tag == "EnemyBear")
        {
            gunLine = this.transform.FindChild("GunRay").GetComponent<LineRenderer>();
        }
        if(this.tag == "EnemyBunny")
        {

        }

        healthScript = GetComponent<EnemyHealth>();

    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (healthScript.isDead == true)
        {
            RemoveAllEffect();
            return;
        }
        setTarget();
        DisableEffect();
        timer += Time.deltaTime;
    }

    void setTarget()
    {
        if (mAnchor.isFiring == true)
        {
            if (tag == "EnemyElephant")
            {
                ElephantAttack();
            }

            if (tag == "EnemyBear")
            {
                BearAttack();
            }
            if (tag == "EnemyBunny")
            {
                BunnyAttack();
            }
        }
        if (mAnchor.isFiring == false)
        {
            nvAgent.enabled = true;
            nvAgent.SetDestination(mAnchor.transform.TransformPoint(relativePos));
        }
    }


    void ElephantAttack()
    {
        float elephantAttackRange = 2.0f;
        if (timer > shootingInterval)
        {
            if ((mAnchor.shootWhichPlayer.transform.position - this.transform.position).magnitude < elephantAttackRange)
            {
                //attack players
                timer = 0;

                //player
                if (mAnchor.shootWhichPlayer.GetComponent<PlayerAttributes>())
                {
                    mAnchor.shootWhichPlayer.GetComponent<PlayerAttributes>().TakeDamage(ElephantDamage);
                }
                //npc
                if (mAnchor.shootWhichPlayer.GetComponent<coop_stats>())
                {
                    mAnchor.shootWhichPlayer.GetComponent<coop_stats>().TakeDamage(ElephantDamage);
                }
                
            }
        }
        nvAgent.enabled = true;
        nvAgent.SetDestination(mAnchor.shootWhichPlayer.transform.position);
    }

    void BearAttack()
    {
        if (CheckVisibility())
        {
            nvAgent.enabled = false;
            Vector3 direction = mAnchor.shootWhichPlayer.transform.position - this.transform.position;
            if (timer > shootingInterval)
            {
                //attack
                timer = 0;
                gunLine.enabled = true;
                gunLine.SetPosition(0, transform.TransformPoint(0.0f, 0.7f, 0.0f));
                gunLine.SetPosition(1, mAnchor.shootWhichPlayer.transform.TransformPoint(0.0f, 1.0f, 0.0f));

                //player
                if (mAnchor.shootWhichPlayer.GetComponent<PlayerAttributes>())
                {
                    mAnchor.shootWhichPlayer.GetComponent<PlayerAttributes>().TakeDamage(BearDamage);
                }
                //npc
                if (mAnchor.shootWhichPlayer.GetComponent<coop_stats>())
                {
                    mAnchor.shootWhichPlayer.GetComponent<coop_stats>().TakeDamage(BearDamage);
                }
            }
           

            direction.y = 0;
            transform.rotation = Quaternion.LookRotation(direction);

        }
        else
        {
            nvAgent.enabled = true;
            nvAgent.SetDestination(mAnchor.shootWhichPlayer.transform.position);
        }
        
    }

    void BunnyAttack()
    {
        float BunnyRange = 20.0f;
        float nearRange = 15.0f;

        if (timer > BunnyMagicTime)
        {
            if((mAnchor.shootWhichPlayer.transform.position - this.transform.position).magnitude > nearRange)
            {
                nvAgent.enabled = true;
                nvAgent.SetDestination(mAnchor.shootWhichPlayer.transform.position);
            }
            else
            {
                nvAgent.enabled = false;
                Vector3 direction = mAnchor.shootWhichPlayer.transform.position - this.transform.position;
                direction.y = 0.0f;
                transform.rotation = Quaternion.LookRotation(direction);
            }

            if ((mAnchor.shootWhichPlayer.transform.position - this.transform.position).magnitude < BunnyRange)
            {
                if (timer > BunnyMagicInterval)
                {
                    timer = 0;
                    BunnyMagicEffect = (GameObject)Instantiate(BunnyMagicEffectPrefab, this.transform.TransformPoint(Vector3.up * 0.8f), this.transform.rotation);
                    MagicCircle = (GameObject)Instantiate(MagicCirclePrefab, mAnchor.shootWhichPlayer.transform.position + Vector3.up * 0.5f, Quaternion.identity);
                    MagicCircle.GetComponent<ParticleSystem>().startSize = 10;
                    nvAgent.enabled = false;
                }
            }    
        }
        else
        {
            nvAgent.enabled = false;
        }
    }

    private bool CheckVisibility()
    {
        bool result = false;
        float maxRange = 10.0f;
        RaycastHit hit;
        Vector3 targetDirection = (mAnchor.shootWhichPlayer.transform.position - this.transform.position).normalized;

        if (Physics.Raycast(transform.position, targetDirection, out hit, maxRange,~LayerMask.GetMask("Enemy")))
        {
            if (hit.transform.gameObject.tag.Contains("Player"))
            {
                result = true;
            }
        }
        return result;
    }


    void DisableEffect()
    {
        if (timer > 0.2f)
        {
            if (gunLine != null)
            {
                gunLine.enabled = false;
            }
        }

        if (timer > BunnyMagicTime)
        {
            if (BunnyMagicEffect != null)
            {
                Destroy(BunnyMagicEffect);
                BunnyMagicEffect = null;

            }
            if (MagicCircle != null)
            {
                GameObject aMeteor = (GameObject)Instantiate(MeteorPrefab, MagicCircle.transform.position + Vector3.up * 10.0f, Quaternion.identity);
                aMeteor.GetComponent<Meteor>().SetTargetPosition(MagicCircle.transform.position);
                aMeteor.GetComponent<Meteor>().damage = BunnyMeteorDamage;
                Destroy(MagicCircle);
                MagicCircle = null;
            }
        }
    }

    void RemoveAllEffect()
    {
        nvAgent.enabled = false;
        if (gunLine != null)
        {
            gunLine.enabled = false;
        }

        if (BunnyMagicEffect != null)
        {
            Destroy(BunnyMagicEffect);
            BunnyMagicEffect = null;

        }
        if (MagicCircle != null)
        {
            Destroy(MagicCircle);
            MagicCircle = null;
        }
    }
}
