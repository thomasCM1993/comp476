﻿using UnityEngine;
using System.Collections;

public class EndScript : MonoBehaviour
{

    bool isDone;
    public GameObject Player;
    float count = 1f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isDone)
        {
            Player.transform.position += Vector3.up * 30 * Time.deltaTime;
            
            count -= Time.deltaTime;
            if (count <= 0)
            {
                Application.LoadLevel(4);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            isDone = true;
        }
    }
}
