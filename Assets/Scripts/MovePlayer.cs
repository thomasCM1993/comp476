using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityStandardAssets.CrossPlatformInput;
public class MovePlayer :  MonoBehaviour{


    public float walkSpeed;
    public float runSpeed;
    public float sprintSpeed;
    public float flySpeed;

    public float turnSmoothing = 3.0f;
    public float aimTurnSmoothing = 15.0f;
    public float speedDampTime = 0.1f;

    public float jumpHeight;
    public float jumpCooldown;

    private float timeToNextJump = 0;

    private float speed;

    private Vector3 lastDirection;

    private Animator anim;
    private Rigidbody rb;
    private int speedFloat;
    private int jumpBool;
    private int hFloat;
    private int vFloat;
    private int aimBool;
    private int flyBool;
    private int groundedBool;
    private bool grounded;
    private Transform cameraTransform;

    private float h;
    private float v;

    private bool aim;

    private bool run;
    private bool sprint=false;

    private bool isMoving;

    // fly
    private bool fly = false;
    private float distToGround;
    private float sprintFactor;

    void Awake()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        cameraTransform = Camera.main.transform;

        speedFloat = Animator.StringToHash("Speed");
        jumpBool = Animator.StringToHash("Jump");
        hFloat = Animator.StringToHash("H");
        vFloat = Animator.StringToHash("V");
        aimBool = Animator.StringToHash("Aim");
        // fly
        flyBool = Animator.StringToHash("Fly");
        groundedBool = Animator.StringToHash("Grounded");
        distToGround = GetComponent<Collider>().bounds.extents.y;
        sprintFactor = sprintSpeed / runSpeed;
    }

    bool IsGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
    }

    void Update()
    {
        aim = true;
        h = CrossPlatformInputManager.GetAxis("Horizontal");
        v = CrossPlatformInputManager.GetAxis("Vertical");
        run = CrossPlatformInputManager.GetButton("Fire3");
        isMoving = Mathf.Abs(h) > 0.1 || Mathf.Abs(v) > 0.1;
    }

    void FixedUpdate()
    {
        AdditionalGravity();
        anim.SetBool(aimBool, IsAiming());
        anim.SetFloat(hFloat, h);
        anim.SetFloat(vFloat, v);
        
        anim.SetBool(groundedBool, IsGrounded());
        MovementManagement(h, v, run, sprint);
        JumpManagement();
        
    }
    void AdditionalGravity()
    {
        GetComponent<Rigidbody>().AddForce(-Vector3.up * 9.8f, ForceMode.Acceleration);
        RaycastHit hit;
        Debug.DrawRay(transform.position, -Vector3.up, Color.green);
        float distanceTOGround = 0.0f;
        if (Physics.Raycast(transform.position, -Vector3.up, out hit))
        {
            distanceTOGround = Vector3.Distance(transform.position, hit.point);
        }
        Debug.Log(distanceTOGround);
    }
    void JumpManagement()
    {
        if (GetComponent<Rigidbody>().velocity.y < 10) // already jumped
        {
            anim.SetBool(jumpBool, false);
            if (timeToNextJump > 0)
                timeToNextJump -= Time.deltaTime;
        }
        if (CrossPlatformInputManager.GetButtonDown("Jump"))
        {
            if (speed >= 0 && timeToNextJump <= 0)
            {
                anim.SetBool(jumpBool, true);
                float jumpVelocity = Mathf.Sqrt(2 * 9.8f * jumpHeight);
                rb.AddForce(Vector3.up*jumpVelocity , ForceMode.VelocityChange);
                timeToNextJump = jumpCooldown;
            }
        }
    }

    void MovementManagement(float horizontal, float vertical, bool running, bool sprinting)
    {
        Rotating(horizontal, vertical);

        if (isMoving)
        {
            if (running)
            {
                speed = runSpeed;
            }
            else
            {
                speed = walkSpeed;
            }

            anim.SetFloat(speedFloat, speed, speedDampTime, Time.deltaTime);
        }
        else
        {
            speed = 0f;
            anim.SetFloat(speedFloat, 0f);
        }
        // rb.AddForce(transform.TransformDirection(Vector3.forward) * speed * vertical);
        // rb.AddForce(transform.TransformDirection(Vector3.right) * speed * horizontal);

        Vector3 velocity = transform.TransformDirection(new Vector3(horizontal, 0, vertical)).normalized * speed;
        rb.velocity = new Vector3(velocity.x, rb.velocity.y, velocity.z); 

    }

    Vector3 Rotating(float horizontal, float vertical)
    {
        Vector3 forward = cameraTransform.TransformDirection(Vector3.forward);
        if (!fly)
            forward.y = 0.0f;
        forward = forward.normalized;

        Vector3 right = new Vector3(forward.z, 0, -forward.x);

        Vector3 targetDirection;

        float finalTurnSmoothing;
        targetDirection = forward;
        finalTurnSmoothing = aimTurnSmoothing;

        if ((isMoving && targetDirection != Vector3.zero) || IsAiming())
        {
            Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);
            Quaternion newRotation = Quaternion.Slerp(GetComponent<Rigidbody>().rotation, targetRotation, finalTurnSmoothing * Time.deltaTime);
            GetComponent<Rigidbody>().MoveRotation(newRotation);
            lastDirection = targetDirection;
        }
        //idle - fly or grounded
        if (!(Mathf.Abs(h) > 0.9 || Mathf.Abs(v) > 0.9))
        {
            Repositioning();
        }

        return targetDirection;
    }

    private void Repositioning()
    {
        Vector3 repositioning = lastDirection;
        if (repositioning != Vector3.zero)
        {
            repositioning.y = 0;
            Quaternion targetRotation = Quaternion.LookRotation(repositioning, Vector3.up);
            Quaternion newRotation = Quaternion.Slerp(GetComponent<Rigidbody>().rotation, targetRotation, turnSmoothing * Time.deltaTime);
            GetComponent<Rigidbody>().MoveRotation(newRotation);
        }
    }

    public bool IsFlying()
    {
        return fly;
    }

    public bool IsAiming()
    {
        return aim && !fly;
    }

    public bool isSprinting()
    {
        return sprint && !aim && (isMoving);
    }
}
