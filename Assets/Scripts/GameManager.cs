﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public GameObject anchorPrefab;

    // Use this for initialization
    void Start () {
        InitializeAnchorPosition();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void InitializeAnchorPosition()
    {
        float targetPointPositionY = 1.5f;

        //anchor1
        int randomNum1 = Random.Range(0, 4);
        GameObject aAnchor1 = (GameObject)Instantiate(anchorPrefab, transform.position, transform.rotation);
        List<Vector3> targetPoints1 = new List<Vector3>();
        targetPoints1.Add(new Vector3(265.0f, targetPointPositionY, 165.0f));
        targetPoints1.Add(new Vector3(233.0f, targetPointPositionY, 165.0f));
        targetPoints1.Add(new Vector3(233.0f, targetPointPositionY, 195.00f));
        targetPoints1.Add(new Vector3(265.0f, targetPointPositionY, 195.0f));
        aAnchor1.GetComponent<EnemyAnchorAI>().SetTargetPoints(targetPoints1);

        aAnchor1.GetComponent<EnemyAnchorAI>().InitializeAnchorPosition(randomNum1);
        aAnchor1.GetComponent<Renderer>().material.color = Color.blue;

        //anchor2
        targetPointPositionY = 16.5f;
        int randomNum2 = Random.Range(0, 4);
        GameObject aAnchor2 = (GameObject)Instantiate(anchorPrefab, transform.position, transform.rotation);
        List<Vector3> targetPoints2 = new List<Vector3>();
        targetPoints2.Add(new Vector3(251.0f, targetPointPositionY, 205.0f));
        targetPoints2.Add(new Vector3(251.0f, targetPointPositionY, 185.0f));
        targetPoints2.Add(new Vector3(249.0f, targetPointPositionY, 185.0f));
        targetPoints2.Add(new Vector3(249.0f, targetPointPositionY, 205.0f));
        aAnchor2.GetComponent<EnemyAnchorAI>().SetTargetPoints(targetPoints2);

        aAnchor2.GetComponent<EnemyAnchorAI>().InitializeAnchorPosition(randomNum2);
        aAnchor2.GetComponent<Renderer>().material.color = Color.blue;

        //anchor3
        targetPointPositionY = 26.5f;
        int randomNum3 = Random.Range(0, 4);
        GameObject aAnchor3 = (GameObject)Instantiate(anchorPrefab, transform.position, transform.rotation);
        List<Vector3> targetPoints3 = new List<Vector3>();
        targetPoints3.Add(new Vector3(260.0f, targetPointPositionY, 170.0f));
        targetPoints3.Add(new Vector3(240.0f, targetPointPositionY, 170.0f));
        targetPoints3.Add(new Vector3(240.0f, targetPointPositionY, 205.00f));
        targetPoints3.Add(new Vector3(260.0f, targetPointPositionY, 205.0f));
        aAnchor3.GetComponent<EnemyAnchorAI>().SetTargetPoints(targetPoints3);

        aAnchor3.GetComponent<EnemyAnchorAI>().InitializeAnchorPosition(randomNum3);
        aAnchor3.GetComponent<Renderer>().material.color = Color.blue;

        //anchor4
        targetPointPositionY = 36.5f;
        int randomNum4 = Random.Range(0, 4);
        GameObject aAnchor4 = (GameObject)Instantiate(anchorPrefab, transform.position, transform.rotation);
        List<Vector3> targetPoints4 = new List<Vector3>();
        targetPoints4.Add(new Vector3(260.0f, targetPointPositionY, 170.0f));
        targetPoints4.Add(new Vector3(240.0f, targetPointPositionY, 170.0f));
        targetPoints4.Add(new Vector3(240.0f, targetPointPositionY, 195.00f));
        targetPoints4.Add(new Vector3(260.0f, targetPointPositionY, 195.0f));
        aAnchor4.GetComponent<EnemyAnchorAI>().SetTargetPoints(targetPoints4);

        aAnchor4.GetComponent<EnemyAnchorAI>().InitializeAnchorPosition(randomNum4);
        aAnchor4.GetComponent<Renderer>().material.color = Color.blue;

        Debug.Log("initialize anchor position");
    }
}
