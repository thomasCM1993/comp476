﻿using UnityEngine;
using System.Collections;

public class TriggerDoor : MonoBehaviour {

    public GameObject Door;
    private bool OpenDoor = false;
    private float countDoor = 10f;
	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(OpenDoor)
        {
            Door.GetComponent<BoxCollider>().isTrigger = true;
            Door.transform.position -= Vector3.up * Time.deltaTime;
            countDoor -= Time.deltaTime;
            if(countDoor <= 0)
            {
                Destroy(Door);
                Destroy(gameObject);
            }
        }
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("inti");
            if(other.GetComponent<PlayerAttributes>().HasGoldKey)
            {
                OpenDoor = true;
                other.GetComponent<PlayerAttributes>().KeyNumber.text = "0";
            }
        }
    }
}
