﻿using UnityEngine;
using System.Collections;

public class PuzzelScript : MonoBehaviour
{
    public bool isCorrectBlock;
    public bool end;
    public GameObject fountan;
    public float fountanCount = 10f;
    public GameObject endZone;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(end)
        {
            OpenEnding();
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Bullet")
        {
            if(isCorrectBlock)
            {
                end = true;
            }
            else
            {
                GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerAttributes>().TakeDamage(30);

            }
            Destroy(collision.collider.gameObject);
        }
    }
    void OpenEnding()
    {
        fountan.transform.position -= Vector3.up * Time.deltaTime;
        endZone.active = true;
        fountanCount += Time.deltaTime;
        if(fountanCount <= 0)
        {
            Destroy(fountan);
            end = false;
        }
    }
}
