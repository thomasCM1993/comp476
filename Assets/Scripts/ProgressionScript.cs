﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class ProgressionScript : MonoBehaviour {

    static public int count = 0;
    static public int countHigherEne = 0;
    public GameObject Portal;

    public Text enemyCount;
	// Use this for initialization
	void Awake ()
    {
        count = 0;
    }
	// Update is called once per frame
	void Update ()
    {
        if (!Portal.GetComponent<Portal>().hasGoneThrough)
        {
            enemyCount.text = "Enemies: " + count;
        }
        else
        {
            enemyCount.text = "Enemies left: " + countHigherEne;
        }
        if (count == 0)
        {
            Portal.GetComponent<Portal>().isActive = true;
        }



    }
    public void IncreaseCount()
    {
        count++;
    }
    public void IncreaseOtherCount()
    {
        countHigherEne++;
    }
    public void DecreaseOtherCount()
    {
        countHigherEne--;
    }
    public void DecreaseCount()
    {
        count--;
    }
}
