﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerAttributes : MonoBehaviour {
    public int currentHealth;
    public const int maxhealth = 100;
    public Slider healthSlider;
    public Image damageImage;
    public float flashSpeed = 5f;                              
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);
    private bool damaged;
    public Transform pointOfRespwan;
    public bool HasGoldKey = false;
    public GameObject Gun;
    public bool isDead;
    public Text KeyNumber;

    private AudioSource source;
    public AudioClip[] clips;
	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
        currentHealth = maxhealth;
        KeyNumber.text = "0";
	}
	
	// Update is called once per frame
	void Update ()
    {
        // If the player has just been damaged...
        if (damaged)
        {
            // ... set the colour of the damageImage to the flash colour.
            damageImage.color = flashColour;
        }
        // Otherwise...
        else
        {
            // ... transition the colour back to clear.
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        if(currentHealth <= maxhealth / 4)
        {
            source.Play();
        }
        else
        {
            //source.Stop();
        }
        // Reset the damaged flag.
        damaged = false;
    }
    void OnCollisionEnter(Collision collision)
    {
        return;
        /*
        if (collision.collider.tag == "GunRay")
        {
            TakeDamage(10);
        }
        if (collision.collider.tag == "EnemyElephant")
        {
            TakeDamage(5);
        }
        if (collision.collider.tag == "EnemyBear" || collision.collider.tag == "EnemyBunny")
        {
            TakeDamage(2);
        }
        */
        
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Health")
        {
			source.PlayOneShot(clips[2]);
            if (currentHealth > 100 && currentHealth + 10 > 100)
            {
                int amount = maxhealth - currentHealth;
                GiveHealth(amount);
            }
            else
            {
                GiveHealth(10);
                
            }
            Destroy(other.gameObject);
        }
        if (other.tag == "GunRay")
        {
            TakeDamage(20);
        }
        if(other.tag == "GoldKey")
        {
			source.PlayOneShot(clips[2]);
            HasGoldKey = true;
            KeyNumber.text = "1";

        }
        if(other.tag == "Ammo")
        {
			source.PlayOneShot(clips[2]);
            Gun.GetComponent<PlayerShoot>().totalPlayerAmmo += 10;
            Destroy(other.gameObject);
        }
    }
    void Death()
    {
        isDead = true;
        Application.LoadLevel(3);
    }
    public void TakeDamage( int amount)
    {
        source.PlayOneShot(clips[0]);
        
        damaged = true;
        
        currentHealth -= amount;

        healthSlider.value = currentHealth;
        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
    }
    public void GiveHealth(int amount)
    {
        currentHealth += amount;
        healthSlider.value = currentHealth;
    }
}
