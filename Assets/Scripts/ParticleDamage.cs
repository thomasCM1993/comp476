﻿using UnityEngine;
using System.Collections;

public class ParticleDamage : MonoBehaviour
{
    public int damage;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<PlayerAttributes>().TakeDamage(damage);
        }
    }
}
