﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;
public class PlayerShoot : MonoBehaviour {

    //just one type of ammo, to make things simple for us
    public int amountOfAmmo;
    public int totalPlayerAmmo;
    public float FireRateTimer;
    public float BulletSpeed;
    //properties that should go in the weapon script but for now are here
    public int weaponDamage;
    public int maxAmmoForWeapon; //maybe not needed
    public int weaponClipSize;
    public float weaponFireRate;
    public float weaponFireRateManx;
    public float weaponReloadRate;
    public Text ammoText;
    AudioSource audio;
    public AudioClip[] sounds;
    // Use this for initialization
    void Start ()
    {
        audio = GetComponent<AudioSource>();
        audio.volume = .2f;
        weaponFireRateManx = weaponFireRate;
        ammoText.text = amountOfAmmo + "/" + totalPlayerAmmo;

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (amountOfAmmo > 0)
        {
            Shoot();
        }
        if (amountOfAmmo != weaponClipSize)
        {
            QuickReload();

        }
	
	}

    void Shoot()
    {
        FireRateTimer -= Time.deltaTime;
        if(CrossPlatformInputManager.GetButton("Fire1") && FireRateTimer <=0)
        {
            //instaniate a bullet near gun tip
            amountOfAmmo--;
            audio.PlayOneShot(sounds[0]);
            GameObject bullet = (GameObject)Instantiate(Resources.Load("Bullet"), transform.position + transform.forward *2 + transform.up/4  , Quaternion.identity);
            GameObject explo = (GameObject)Instantiate(Resources.Load("Explosion2"), transform.position + transform.forward * 2, Quaternion.identity);
            //lets say all bullets travel at the same speed and do not accelerate or deccelerate during there travel
            bullet.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * BulletSpeed);
            FireRateTimer = weaponFireRateManx;
            Debug.Log(FireRateTimer);
        }
        if(FireRateTimer < -100)
        {
            FireRateTimer = 0;
        }
        ammoText.text = amountOfAmmo + "/" + totalPlayerAmmo;
    }
    void FixedUpdate()
    {
        transform.LookAt(Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 100f)));

    }
    void QuickReload()
    {
        if (totalPlayerAmmo > 0)
        {
            if (CrossPlatformInputManager.GetButton("Fire2") || amountOfAmmo <= 0)
            {
                
                StartCoroutine(Reload());
            }
        }
    }
    IEnumerator Reload()
    {
        
        yield return new WaitForSeconds(weaponReloadRate);
        
        int amountToClipSize = weaponClipSize - amountOfAmmo;
        if(totalPlayerAmmo - amountToClipSize < 0)
        {
            amountToClipSize = totalPlayerAmmo;
        }
        totalPlayerAmmo -= amountToClipSize;
        amountOfAmmo += amountToClipSize;
        ammoText.text = amountOfAmmo + "/" + totalPlayerAmmo;
        //audio.PlayOneShot(sounds[1]);

    }
}
