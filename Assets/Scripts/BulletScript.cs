﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour {
    public int damage;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Contains("Enemy"))
        {
            other.GetComponent<EnemyHealth>().TakeDamage(damage, other.ClosestPointOnBounds(this.transform.position));
        }
        Destroy(gameObject);
    }
}
