﻿using UnityEngine;
using System.Collections;

public class Portal : MonoBehaviour {

    //Vector3 targetAngles;
    public bool isActive = false;
    public bool hasGoneThrough = false;
    public GameObject npc_coop_ref;
	void OnTriggerEnter(Collider col) {

		if (col.gameObject.tag == "Player") 
		{
			Debug.Log ("TELEPORTED");
            //targetAngles = col.transform.eulerAngles + 180f * Vector3.up;
            if (isActive)
            {
                if (this.gameObject.tag == "Portal3to4")
                {
                    hasGoneThrough = true;
                    col.transform.position = new Vector3(250.0f, 36.0f, 200.0f);
                    if (npc_coop_ref == null)
                    {
                        npc_coop_ref = GameObject.FindGameObjectWithTag("PlayerCoop");
                    }
                    if (npc_coop_ref != null)
                    {
                        Vector3 v = new Vector3(252.0f, 36.0f, 200.0f);
                        npc_coop_ref.GetComponent<NavMeshAgent>().Warp(v);
                    }

                }
                if (this.gameObject.tag == "Portal4to3")
                {

                    col.transform.position = new Vector3(250.0f, 26.0f, 163.0f);


                    if (npc_coop_ref == null)
                    {
                        npc_coop_ref = GameObject.FindGameObjectWithTag("PlayerCoop");
                    }
                    if (npc_coop_ref != null)
                    {
                        Vector3 v = new Vector3(250.0f, 26.0f, 165.0f);
                        npc_coop_ref.GetComponent<NavMeshAgent>().Warp(v);
                    }

                }
            }
		}
	}
}
