﻿using UnityEngine;
using System.Collections;

public class coop_stats : MonoBehaviour {





    public int currentHealth;
    public const int maxhealth = 100;
   // public Slider healthSlider;
   // public Image damageImage;
   // public float flashSpeed = 5f;
   // public Color flashColour = new Color(1f, 0f, 0f, 0.1f);
    private bool damaged;
    public Transform pointOfRespwan;
   // public bool HasGoldKey = false;
    public GameObject Gun;
    public bool isDead;
    NavMeshAgent navAgentRef;
  

    private AudioSource source;
    public AudioClip[] clips;
    // Use this for initialization
    void Start()
    {
        source = GetComponent<AudioSource>();
        currentHealth = maxhealth;
        navAgentRef = GetComponent<NavMeshAgent>();
       // KeyNumber.text = "0";
    }

    // Update is called once per frame
    void Update()
    {
        // If the player has just been damaged...
        if (damaged)
        {
            // ... set the colour of the damageImage to the flash colour.
           // damageImage.color = flashColour;
        }
        // Otherwise...
        else
        {
            // ... transition the colour back to clear.
           // damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        if (currentHealth <= maxhealth / 4)
        {
            source.Play();
        }
        else
        {
            //source.Stop();
        }
        // Reset the damaged flag.
        damaged = false;
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "GunRay")
        {
            TakeDamage(10);
        }
        if (collision.collider.tag == "EnemyElephant")
        {
            TakeDamage(5);
        }
        if (collision.collider.tag == "EnemyBear" || collision.collider.tag == "EnemyBunny")
        {
            TakeDamage(2);
        }

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "GunRay")
        {
            TakeDamage(20);
        }

    }
    void Death()
    {
        // isDead = true;
        // Application.LoadLevel(3);
        respawn();
    }
    public void TakeDamage(int amount)
    {
        //source.PlayOneShot(clips[0]);

        damaged = true;

        currentHealth -= amount;

       // healthSlider.value = currentHealth;
        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
    }
    public void GiveHealth(int amount)
    {
        currentHealth += amount;
       // healthSlider.value = currentHealth;
    }
    void respawn() {
        currentHealth = maxhealth;
        navAgentRef.Warp(pointOfRespwan.position);
    }
}
