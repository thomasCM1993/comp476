﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
//u
public class coop_tactics : MonoBehaviour
{
    GameObject[] bears;// = GameObject.FindGameObjectsWithTag("EnemyBear");
    GameObject[] bunnies;// = GameObject.FindGameObjectsWithTag("EnemyBunny");
    GameObject[] elephants;// = GameObject.FindGameObjectsWithTag("EnemyElephant");
    GameObject target;
    public List<GameObject> enemies;
    GameObject playerRef;
    coop_shoot shootRef;
    bool inited = false;
    // Use this for initialization
    void Start()
    {
        preInitEnemyList();
        initEnemyList();
        playerRef = GameObject.FindGameObjectWithTag("Player");
        shootRef = GetComponentInChildren<coop_shoot>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!inited) {
            preInitEnemyList();
            initEnemyList();
            if (enemies.Count > 1) {
                inited = true;
            }
        }

       // updateEnemyList();

    }
    void preInitEnemyList()
    {
        bears = GameObject.FindGameObjectsWithTag("EnemyBear");
        bunnies = GameObject.FindGameObjectsWithTag("EnemyBunny");
        elephants = GameObject.FindGameObjectsWithTag("EnemyElephant");
    }
    void initEnemyList()
    {
        foreach (GameObject g in bears)
        {
            enemies.Add(g);
        }
        foreach (GameObject g in bunnies)
        {
            enemies.Add(g);
        }
        foreach (GameObject g in elephants)
        {
            enemies.Add(g);
        }

    }
    public void updateEnemyList()
    {
        bool done = false;
        while (!done)
        {
            done = true;
            target = null;
            foreach (GameObject g in enemies)
            {
                if (g == null) { 
                    enemies.Remove(g);
                    done = false;
                    break;
                }
                else
                {
                    if (target == null)
                    {
                        target = g;
                    }
                    else
                    {
                        if (Vector3.Distance(g.transform.position, this.transform.position) < Vector3.Distance(target.transform.position, this.transform.position))
                        {
                            target = g;
                        }
                    }

                }
            }
            if(done)
            shootRef.targetRef = target;
            //shootRef.Shoot();
        }
    }
}
