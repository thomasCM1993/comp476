﻿using UnityEngine;
using System.Collections;
//u
public class coop_shoot : MonoBehaviour {
    //just one type of ammo, to make things simple for us
  //  public int amountOfAmmo;
  //  public int totalPlayerAmmo;
    public float FireRateTimer;
    public float BulletSpeed;
    //properties that should go in the weapon script but for now are here
    public int weaponDamage;
   // public int maxAmmoForWeapon; //maybe not needed
   // public int weaponClipSize;
    public float weaponFireRate;
    public float weaponFireRateManx;
    public Vector3 aimTarget { get; set; }
    // public float weaponReloadRate;
    // public Text ammoText;
    AudioSource audio;
    public AudioClip[] sounds;
    public GameObject targetRef { get; set; }
    public coop_tactics tacRef;
    public coop_move movRef;

    bool fire = false;
    // Use this for initialization
    void Start () {
        audio = GetComponent<AudioSource>();
        audio.volume = .2f;
        weaponFireRateManx = weaponFireRate;
        targetRef = GameObject.FindGameObjectWithTag("Player");
        tacRef = GetComponentInParent<coop_tactics>();
        movRef = GetComponentInParent<coop_move>();
    }
	
	// Update is called once per frame
	void Update () {
        tacRef.updateEnemyList();
        if (targetRef == null)
        {
        }
        else
        {
            transform.LookAt(targetRef.transform.position);
            if(movRef.escaped)
            Shoot();
        }
        if (FireRateTimer > 0)
        {
            FireRateTimer -= Time.deltaTime;
        }
    }


    public void Shoot()
    {
 
        //print("coop fire:" + CheckVisibility());
        if ( CheckVisibility( ) && (FireRateTimer <= 0))
        {
            FireRateTimer = weaponFireRateManx;
            //instaniate a bullet near gun tip
            // amountOfAmmo--;
            audio.PlayOneShot(sounds[0]);
            GameObject bullet = (GameObject)Instantiate(Resources.Load("Bullet"), transform.position + transform.forward * 2 + transform.up / 4, Quaternion.identity);
            GameObject explo = (GameObject)Instantiate(Resources.Load("Explosion2"), transform.position + transform.forward * 2, Quaternion.identity);
            //lets say all bullets travel at the same speed and do not accelerate or deccelerate during there travel
            bullet.GetComponent<Rigidbody>().AddForce(this.transform.forward * BulletSpeed);
            FireRateTimer = weaponFireRateManx;
            Debug.Log(FireRateTimer);
        }
       
        
        //ammoText.text = amountOfAmmo + "/" + totalPlayerAmmo;
    }
    
    void FixedUpdate()
    {
      

    }
    private bool CheckVisibility()
    {
        bool result = false;
        float maxRange = 10.0f;
        RaycastHit hit;
        Vector3 targetDirection = (targetRef.transform.position - this.transform.position).normalized;

        if (Physics.Raycast(transform.position, targetDirection, out hit, maxRange))
        {
            if (hit.transform.gameObject.tag.Contains("Enemy"))
            {
                result = true;
            }
        }
        return result;
    }
}
