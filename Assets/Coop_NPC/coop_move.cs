﻿using UnityEngine;
using System.Collections;

public class coop_move : MonoBehaviour {
    public bool escaped = false;
    NavMeshAgent navAgentRef;
    GameObject playerRef;
    private Animator anim;


    private int speedFloat;
    private int jumpBool;
    private int hFloat;
    private int vFloat;
    private int aimBool;
    private int flyBool;
    private int groundedBool;
    private bool grounded;


    private float h;
    private float v;

    private bool aim;

    private bool run;
    private bool sprint = false;

    private bool isMoving;

    // fly
    private bool fly = false;
    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        navAgentRef = gameObject.GetComponent<NavMeshAgent>();
        playerRef = GameObject.FindGameObjectWithTag("Player");



        anim = GetComponent<Animator>();


        speedFloat = Animator.StringToHash("Speed");
        jumpBool = Animator.StringToHash("Jump");
        hFloat = Animator.StringToHash("H");
        vFloat = Animator.StringToHash("V");
        aimBool = Animator.StringToHash("Aim");
        // fly
        flyBool = Animator.StringToHash("Fly");
        groundedBool = Animator.StringToHash("Grounded");
    }
	
	// Update is called once per frame
	void Update () {
        if(escaped)
        updateNav();
        else if (Vector3.Distance(this.transform.position, playerRef.transform.position) < 3.0f)
        {
            escaped = true;
            gameObject.tag = "PlayerCoop";
        }

    }
    void FixedUpdate() {
      //  AdditionalGravity();
        anim.SetBool(aimBool, true);
        anim.SetFloat(hFloat, navAgentRef.velocity.x);
        anim.SetFloat(vFloat, navAgentRef.velocity.z);

        anim.SetBool(groundedBool, true);
        anim.SetFloat(speedFloat, navAgentRef.velocity.magnitude, 0.1f, Time.deltaTime);
        // MovementManagement(h, v, run, sprint);
        //JumpManagement();
       // print("coop velo: "+navAgentRef.velocity);
    }

    void updateNav() {
        if (Vector3.Distance(this.transform.position, playerRef.transform.position) < 15.0f) { 
            Vector3 offset = new Vector3(2f, 0f, 0f);
            navAgentRef.SetDestination(playerRef.transform.position + offset);
        }
    }
}
